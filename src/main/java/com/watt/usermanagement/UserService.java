package com.watt.usermanagement;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UserService {

    private static ArrayList<User> userList = new ArrayList<>();
    private static User currentUser = null;
    private static User superAdmin = new User("super","super");
    static {
    }

    // create
    public static boolean addUser(User user) {
        userList.add(user);
        save();
        return true;
    }

    // Delete
    public static boolean delUser(User user) {
        userList.remove(user);
        save();
        return true;
    }

    public static boolean delUser(int index) {
        userList.remove(index);
        save();
        return true;
    }

    // Read
    public static ArrayList<User> getUsers() {
        return userList;
    }

    public static User getUser(int index) {
        return userList.get(index);
    }

    // Update
    public static boolean updateUser(int index, User user) {
        userList.set(index, user);
        save();
        return true;
    }

    public static void save() {
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            file = new File("user.bin");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(userList);
            oos.close();
            fos.close();
        }catch (FileNotFoundException  ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }catch (IOException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void load() {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            file = new File("user.bin");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            userList = (ArrayList<User>)ois.readObject();
            ois.close();
            fis.close();
        }catch (FileNotFoundException  ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static User login(String userName, String password) {
        if(userName.equals("super") && password.equals("super")) {
            return superAdmin;
        }
        for (int i = 0; i < userList.size(); i++) {
            User user = userList.get(i);
            if (user.getUsername().equals(userName) && user.getPassword().equals(password)) {
                currentUser = user;
                return user;
            }
        }
        return null;
    }

    public static boolean isLogin() {
        return currentUser != null;
    }

    public static User getCurrentUser() {
        return currentUser;
    }
    public static void logout() {
        currentUser = null;
    }
}
